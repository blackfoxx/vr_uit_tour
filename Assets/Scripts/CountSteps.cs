﻿using UnityEngine;
using UnityEngine.UI;

public class StepCounter : MonoBehaviour
{

    public Text locaX, locaY, locaZ, TStep, V;
    private Vector3 load;
    private float locationX, locationY, locationZ;
    private float vBehind = 0f, vNow = 0f;
    private float yBehind = 0f, yNow = 0f;
    private int steps = 0;
    private void Start()
    {

    }

    private void Update()
    {
        LoadAccX();
        //Debug.Log(Input.compensateSensors);

    }

    private void LoadAccX()
    {
        if (Input.accelerationEventCount > 0) //Xác định có gia tốc
        {
            load = Input.acceleration;
            locationX = load.x;
            locationY = load.y;
            locationZ = load.z;

            locaX.text = "x " + locationX.ToString();
            locaY.text = "y " + locationY.ToString();
            locaZ.text = "z " + locationZ.ToString();

            vNow = load.magnitude;
            yNow = locationY;
            V.text = vNow.ToString();
            Debug.Log(Mathf.Abs(yNow - yBehind));

            if ((Mathf.Abs(vNow - vBehind) > 0.12) && (Mathf.Abs(yNow - yBehind) > 0.02))
            {
                steps++;
                TStep.text = steps.ToString();
            }

            yBehind = yNow;
            vBehind = vNow;
        }
    }
}