﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Đây là code load thanh bar theo thời gian. Khi di chuyển point về ảnh thanh bar sẽ được di chuyển. 
//kết thúc thanh bar, sẽ gọi hàm ObBarFill() để chuyển tới cảnh tiếp theo.

public class SliderVR : MonoBehaviour {

    public float fillTime = 2f;

    private Slider mySlider;
    private float timer;
    private bool grazeAt;
    private Coroutine fillBarRoutine;

    public GameObject UpFloor;
    //private SphereChanger Call;

	// Use this for initialization
	void Start () {
        mySlider = GetComponent<Slider>();
        if (mySlider == null)
            Debug.Log("Add slider to this gameobject");
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void PointerEnterETown()
    {
        grazeAt = true;
        fillBarRoutine = StartCoroutine(FillBarE());
    }

    public void PointerEnterATown()
    {
        grazeAt = true;
        fillBarRoutine = StartCoroutine(FillBarA());
    }

    public void PointerEnterCTown()
    {
        grazeAt = true;
        fillBarRoutine = StartCoroutine(FillBarC());
    }

    public void PointerEnterSettingTown()
    {
        grazeAt = true;
        fillBarRoutine = StartCoroutine(FillBarSetting());
    }

    public void PointerEnterUpFloor()
    {
        grazeAt = true;
        fillBarRoutine = StartCoroutine(FillBarUpFloor());
    }


    public void PointerEnterExit()
    {
        grazeAt = true;
        fillBarRoutine = StartCoroutine(FillBarExit());
    }

    public void PointerExit()
    {
        grazeAt = false;
        if (fillBarRoutine != null)
        {
            StopCoroutine(fillBarRoutine);
        }
        timer = 0f;
        mySlider.value = 0f;
    }

    private IEnumerator FillBarA()
    {
        timer = 0f;
        while(timer < fillTime)
        {
            timer += Time.deltaTime;

            mySlider.value = timer / fillTime;

            yield return null;

            if (grazeAt)
                continue;

            timer = 0f;
            mySlider.value = 0f;
            yield break;
        }
        SphereChanger.Instance.LoadATown();
    }

    private IEnumerator FillBarE()
    {
        timer = 0f;
        while (timer < fillTime)
        {
            timer += Time.deltaTime;

            mySlider.value = timer / fillTime;

            yield return null;

            if (grazeAt)
                continue;

            timer = 0f;
            mySlider.value = 0f;
            yield break;
        }
        SphereChanger.Instance.LoadETown();
    }

    private IEnumerator FillBarC()
    {
        timer = 0f;
        while (timer < fillTime)
        {
            timer += Time.deltaTime;

            mySlider.value = timer / fillTime;

            yield return null;

            if (grazeAt)
                continue;

            timer = 0f;
            mySlider.value = 0f;
            yield break;
        }
        SphereChanger.Instance.LoadCTown();
    }

    private IEnumerator FillBarSetting()
    {
        timer = 0f;
        while (timer < fillTime)
        {
            timer += Time.deltaTime;

            mySlider.value = timer / fillTime;

            yield return null;

            if (grazeAt)
                continue;

            timer = 0f;
            mySlider.value = 0f;
            yield break;
        }
        SphereChanger.Instance.LoadSetting();
    }

    private IEnumerator FillBarUpFloor()
    {
        timer = 0f;
        while (timer < fillTime)
        {
            timer += Time.deltaTime;

            mySlider.value = timer / fillTime;

            yield return null;

            if (grazeAt)
                continue;

            timer = 0f;
            mySlider.value = 0f;
            yield break;
        }
        SphereChanger.Instance.LoadUpFloor(UpFloor);
    }

    private IEnumerator FillBarExit()
    {
        timer = 0f;
        while (timer < fillTime)
        {
            timer += Time.deltaTime;

            mySlider.value = timer / fillTime;

            yield return null;

            if (grazeAt)
                continue;

            timer = 0f;
            mySlider.value = 0f;
            yield break;
        }
        Application.Quit();
    }

}
