﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Class để tạo cho sphere
//Chỉ làm trên 4 hướng chính, là (trước, sau, trái, phải)
//Nếu không có thì bỏ trống
//Cái này rất quan trọng vì khi add sai sphere sẽ dần đến sai hình, gây lỗi cho trải nghiệm người dùng --> Bị trừ điểm
//Phần này RẤT RẤT QUAN TRỌNG, NÊN MỌI NGƯỜI CHÚ Ý
public class ObjectScene : MonoBehaviour {
    public GameObject frontScene;
    public GameObject behindScene;
    public GameObject rightScene;
    public GameObject leftScene;
}
