﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereChanger : MonoBehaviour {

    public static SphereChanger Instance { set; get; }

    private struct MoveDir //Tạo một struct để kiểm tra xem sphere đang được di chuyển về hướng nào
    {
        public bool x;
        public bool z;
    }

    //This object should be called 'Fader' and placed over the camera
    GameObject m_Fader;

    //This ensures that we don't mash to change spheres
    bool changing = false;

    public GameObject ETown;
    public GameObject ATown;
    public GameObject CTown;

    public GameObject First;
    public GameObject Wellcome;
    public GameObject Setting;

    public Camera MainCamera; //Kiểm soát camera, cái này chỉ dùng để lấy được rotation của nó

    private Quaternion orientation = Quaternion.Euler(0, 0, 0); //Xác đinh góc quay của Sphere
   
    private GameObject ChooseScene; //Sphere đang hiện hữu trên màn hình

    //Var for count steps
    //public Text locaX, locaY, locaZ, TStep, V;
    private Vector3 load;  //Lấy được góc của device, dùng cho count steps
    private float locationX, locationY, locationZ; //Lấy từng độ của device
    private float vBehind = 0f, vNow = 0f; //Trạng thái trước của device
    private float yBehind = 0f, yNow = 0f; //Trạng thái sau của device

    private MoveDir ChooseMove; //Biến kiểm tra sphere đi về hướng nào

    private bool SceneIsWellcome_Setting;

    private void Start()
    {
        Instance = this;
        //Khởi tạo cho sphere đầu tiên, đứng ở đầu tiên trong list round
        ChooseScene = Instantiate(First);
        ChooseScene.transform.localRotation = Quaternion.Euler(-4, 93, 0);
        SceneIsWellcome_Setting = true;

        ChooseMove.x = true;
        ChooseMove.z = true;
        //CreateScene();
    }

    private void Update()
    {
        if (SceneIsWellcome_Setting == false)
        {
            //Di chuyển theo bước chân, dùng trong device
            MoveByCountSteps();

            //Test app trong unity, dùng phím "UP"
            MoveByKeyBoard();
        }
    }

    //Hàm kiểm tra theo bàn phím, sử dụng phím up
    private void MoveByKeyBoard()
    {
        //Lấy sự kiện bàn phím được nhấn "UP"
        if (Input.GetKeyDown("up"))
        {
            //Gọi hàm tịnh tiến và load sphere tiếp theo
            LoadNextScene();
        }
    }

    //Di chuyển theo countsteps, count steps này có vẻ nhạy so với bước đi, mỗi lần user di chuyển chắc chắn cảnh sẽ được di chuyển
    private void MoveByCountSteps()
    {
        if (Input.accelerationEventCount > 0) //Xác định có gia tốc, kiểm tra xem có sự di chuyển nào không
        {
            //Dùng sensor accleration trong device, sử dụng trên android và cả ios, chưa thử trên windows phone
            load = Input.acceleration;
            locationX = load.x;
            locationY = load.y;
            locationZ = load.z;

            vNow = load.magnitude;  //Lấy độ lớn vector, tính theo eciulic
            yNow = locationY;
            //Debug.Log(Mathf.Abs(yNow - yBehind));

            //Xử lý count steps. Vì device nằm ngang nên chỉ cần xử lý 1 cử chỉ là được, dùng theo trục y. 
            //Nêu trong app count steos thì phải xử lý hết tất cả các chiều x, y, z
            if ((Mathf.Abs(vNow - vBehind) > 0.12) && (Mathf.Abs(yNow - yBehind) > 0.02))
            {
                //Gọi hàm chuyển cảnh và move
                LoadNextScene();
            }

            //Gán trạng thái hiện tại cho Behind, xử lý của count steps để kiểm tra cho trạng thái tiếp
            yBehind = yNow;
            vBehind = vNow;
        }
    }

    private void LoadNextScene()
    {
        float MAXMINSIZE = 0.5f;

        float MoveX;
        float MoveZ;

        MoveX = (-Camera.main.transform.forward * 0.15f).x;
        MoveZ = (-Camera.main.transform.forward * 0.15f).z;

        //Kiểm tra xem sphere có bị trượt quá mức giới hạn chưa, 
        //nếu trượt quá mức về phía (trước, sau, trái, phải) thì chuyển về sphere tương ứng, nêu có
        
        CheckLocationFutureChangeScene(MoveX, MoveZ);
        
        if ((MoveX != 0) && (ChooseMove.x == false))
        {
            ObjectScene temp = ChooseScene.GetComponent<ObjectScene>();
            if ((ChooseScene.transform.localPosition.x > MAXMINSIZE) && (temp.frontScene != null))
            {
                Debug.Log("Load next frontScene");
                //Destroy(ChooseScene);
                CreateScene(temp.frontScene);
            }
            else if ((ChooseScene.transform.localPosition.x < -MAXMINSIZE) && (temp.behindScene != null))
            {
                Debug.Log("Load next frontScene");
                //Destroy(ChooseScene);
                CreateScene(temp.behindScene);
            }

            return;  
        }
        if ((MoveZ != 0) && (ChooseMove.z == false))
        {
            ObjectScene temp = ChooseScene.GetComponent<ObjectScene>();
            if ((ChooseScene.transform.localPosition.z > MAXMINSIZE) && (temp.rightScene != null))
            {
                Debug.Log("Load next rightScene");
                //ChooseScene);
                CreateScene(temp.rightScene);
            }
            else if ((ChooseScene.transform.localPosition.z < -MAXMINSIZE) && (temp.leftScene != null))
            {
                Debug.Log("Load next leftScene");
                //Destroy(ChooseScene);
                CreateScene(temp.leftScene);
            }
            return;
        }

        StartCoroutine(TranformInSphere(new Vector3(MoveX, 0, MoveZ)));
    }

    private IEnumerator TranformInSphere(Vector3 destination)
    {
        Vector3 distance = destination / 6;

        for (int i = 0; i < 5; i++)
        {
            ChooseScene.transform.localPosition += distance;
            yield return new WaitForSeconds(0.03f);
        }
    }

    //Hàm kiểm tra Sphere có bị trượt quá hay không, trả về cho ChooseMove giá trị true, false
    private void CheckLocationFutureChangeScene(float MoveX, float MoveZ)
    {
        float TempX = ChooseScene.transform.localPosition.x + MoveX;
        float TempZ = ChooseScene.transform.localPosition.z + MoveZ;

        float MAXMINSIZE = 0.65f;

        if ((TempX > MAXMINSIZE) || (TempX < -MAXMINSIZE))
        {
            ChooseMove.x = false;
        }
        else
        {
            ChooseMove.x = true;
        }
        if ((TempZ > MAXMINSIZE) || (TempZ < -MAXMINSIZE))
        {
            ChooseMove.z = false;
        }
        else
        {
            ChooseMove.z = true;
        }
    }

    void Awake()
    {
        //Find the fader object
        m_Fader = GameObject.Find("Fader");

        //Check if we found something
        if (m_Fader == null)
            Debug.LogWarning("No Fader object found on camera.");
    }

    //Hàm kết thúc app, về hệ chính
    public void Exit()
    {
        Application.Quit();
    }

    public void CreateScene(GameObject next)
    {
        //Start the fading process
        StartCoroutine(FadeCamera(next));
    }
    
    IEnumerator FadeCamera(GameObject next)
    {
        StartCoroutine(FadeIn(0.2f, m_Fader.GetComponent<Renderer>().material));
        yield return new WaitForSeconds(0.31f);
        if (ChooseScene != null)
        {
            Destroy(ChooseScene);
            
            //ChooseScene = null;
        }
        yield return new WaitForSeconds(0.001f);

        ChooseScene = Instantiate(next) as GameObject;
        ChooseScene.transform.localPosition = new Vector3(0, 0, 0);
        ChooseScene.transform.localRotation = next.transform.rotation;
        //ChooseScene.transform.localRotation = Quaternion.Euler(-4, 0, 0);

        StartCoroutine(FadeOut(0.2f, m_Fader.GetComponent<Renderer>().material));
    }
    
    IEnumerator FadeOut(float time, Material mat)
    {
        //While we are still visible, remove some of the alpha colour
        while (mat.color.a > 0.0f)
        {
            mat.color = new Color(mat.color.r, mat.color.g, mat.color.b, mat.color.a - (Time.deltaTime / time));
            yield return null;
        }
    }

    IEnumerator FadeIn(float time, Material mat)
    {
        //While we aren't fully visible, add some of the alpha colour
        while (mat.color.a < 1.0f)
        {
            mat.color = new Color(mat.color.r, mat.color.g, mat.color.b, mat.color.a + (Time.deltaTime / time));
            yield return null;
        }
    }

    public void LoadETown()
    {
        SceneIsWellcome_Setting = false;
        Destroy(ChooseScene);
        CreateScene(ETown);
        Debug.Log("Load E Town");
    }

    public void LoadATown()
    {
        SceneIsWellcome_Setting = false;
        Destroy(ChooseScene);
        CreateScene(ATown);
        Debug.Log("Load A Town");
    }

    public void LoadCTown()
    {
        SceneIsWellcome_Setting = false;
        Destroy(ChooseScene);
        CreateScene(CTown);
        Debug.Log("Load C Town");
    }

    public void LoadSetting()
    {
        SceneIsWellcome_Setting = true;
        Destroy(ChooseScene);
        CreateScene(Setting);
        Debug.Log("Load Setting");
    }

    public void LoadUpFloor(GameObject UpFloor)
    {
        SceneIsWellcome_Setting = false;
        Destroy(ChooseScene);
        CreateScene(UpFloor);
        Debug.Log("Load Upfloor Town");
    }
}